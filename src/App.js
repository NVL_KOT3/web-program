import React, { Component } from 'react'
import './App.css'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { withRouter } from 'react-router-dom'
import Header from './components/header'
import Home from './pages/home'
import Tournaments from './pages/tournaments'
import OrgCompany from './pages/organizations'
import Biography from './pages/biography'
import Prize from './pages/prize'
import Game from './pages/game'
import './css/adaptive/main.css'

class App extends Component {
  render() {
    return (
      <div className="">
        <Router history={this.props.history}>
          {/* Место для header */}
          <Header />

          <Switch>
            <Route path="/game" component={Game} />
            <Route path="/prize" component={Prize} />
            <Route path="/biography" component={Biography} />
            <Route path="/tournaments" component={Tournaments} />
            <Route path="/organizations" component={OrgCompany} />
            <Route path="/" component={Home} />
          </Switch>
          {/* Место для Footer */}
        </Router>
      </div>
    )
  }
}

export default withRouter(App)
