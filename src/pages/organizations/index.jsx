import React from 'react'
import '../../css/main.css'
import '../../css/org.css'
import '../../css/adaptive/adaptive_organizations.css'
import LeftBlock from './components/left-block'
import RightBlock from './components/right-block'
import og from '../../img/org/og.png'
import tl from '../../img/org/tl.png'
import lgd from '../../img/org/lgd.png'
import eg from '../../img/org/eg.png'
import tspirit from '../../img/org/ts.png'
import vp from '../../img/org/vp.png'
import tsecret from '../../img/org/tsecret.png'
import newbee from '../../img/org/newbee.png'
import fnatic from '../../img/org/fnatic.png'
import navi from '../../img/org/navi.png'


const OrgCompany = () => {
	return (
		<section>
			<div className="container">
				<center>
					<h4>
						ТОП 10 ОРГАНИЗАЦИЙ ПО СУММЕ ПРИЗОВЫХ
					</h4>
				</center>
					<LeftBlock
						img={og}
						name={'OG'}
						country={'Европа'}
						years={'в 2015 году.'}
						prise={'$34 590 573'}
						site={'http://ogs.gg'}
						link={'http://ogs.gg'}
					/>
					<RightBlock
						img={tl}
						name={'Team Liquid'}
						country={'Нидерланды (Голландия)'}
						years={'в 2000 году.'}
						prise={'$29 248 097'}
						site={'http://www.teamliquidpro.com'}
						link={'http://www.teamliquidpro.com'}
					/>
					<LeftBlock
						img={lgd}
						name={'LGD Gaming'}
						country={'Китай'}
						years={'в 2009 году.'}
						prise={'$22 222 972'}
						site={'http://e.t.qq.com/lgdgaming'}
						link={'http://e.t.qq.com/lgdgaming'}
					/>
					<RightBlock
						img={eg}
						name={'Evil Geniuses'}
						country={'США'}
						years={'в 1999 году.'}
						prise={'$21 748 879'}
						site={'http://evilgeniuses.gg'}
						link={'http://evilgeniuses.gg'}
					/>
					<LeftBlock
						img={tspirit}
						name={'Team Spirit'}
						country={'Россия'}
						years={'в 2015 году.'}
						prise={'$18 530 273'}
						site={'http://teamspirit.ru'}
						link={'http://teamspirit.ru'}
					/>
					<RightBlock
						img={vp}
						name={'Virtus.pro'}
						country={'Россия'}
						years={'в 2003 году.'}
						prise={'$15 395 673'}
						site={'http://virtus.pro'}
						link={'http://virtus.pro'}
					/>
					<LeftBlock
						img={tsecret}
						name={'Team Secret'}
						country={'Европа'}
						years={'в 2014 году.'}
						prise={'$14 895 275'}
						site={'http://www.teamsecret.gg'}
						link={'http://www.teamsecret.gg'}
					/>
					<RightBlock
						img={newbee}
						name={'NewBee'}
						country={'Китай'}
						years={'в 2014 году.'}
						prise={'$12 022 325'}
						site={'http://t.qq.com/nbgaming'}
						link={'http://t.qq.com/nbgaming'}
					/>
					<LeftBlock
						img={fnatic}
						name={'Fnatic'}
						country={'Великобритания'}
						years={'в 2004 году.'}
						prise={'$11 761 864'}
						site={'http://www.fnatic.com'}
						link={'http://www.fnatic.com'}
					/>
					<RightBlock
						img={navi}
						name={'Natus Vincere'}
						country={'Украина'}
						years={'в 2009 году.'}
						prise={'$11 511 079'}
						site={'http://navi-gaming.com'}
						link={'http://navi-gaming.com'}
					/>
			</div>
	</section>
	)
}

export default OrgCompany
