import React from 'react'

const RightBlock = ({ name, country, years, prise, site, img, link }) => {
	return (
		<div className="divright">
			<img src={img} alt="" className="leftimg" />
			<p>
				Название: {name}
				<br />
				Страна: {country} 
				<br />
				Основан: {years}
				<br />
				Призовые: {prise}
				<br />
				Сайт: <a target='_blank' href={link}>{site}</a>
			</p>
		</div>
	)
}

export default RightBlock