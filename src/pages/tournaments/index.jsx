import React from 'react'

import '../../css/main.css'
import '../../css/adaptive/adaptive_tournaments.css'
import BlockMain from './components/block-main'

const Tournaments = () => {
	return (
		<BlockMain/>
	)
}

export default Tournaments