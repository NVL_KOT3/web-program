import React from 'react'

import navi from '../../../img/navi.png'
import spirit from '../../../img/spirit.png'

const BlockMain = () => {
	return (
		<section className="about">
			<div className="container">
				<div className="navi">
					<img src={navi} alt="navi" className="navipng" />
					<div className="text-navi">
						<p>
					 		Natus Vincere стали чемпионами PGL Major Stockholm 2021 по counter-strike: global offensive.
					 	</p>
					 	<p>
							Natus Vincere победили на PGL Major Stockholm 2021 по CS:GO. В финале они обыграли G2 Esports со счетом 2:0 — 16:11 на Ancient и 22:19 на Nuke. За первое место коллектив Александра s1mple Костылева заработал миллион долларов.
						</p>
						<p>
							Обе команды вышли в плей-офф со статистикой 3:0 в стадии New Legends. В четвертьфинале G2 Esports обыграла Ninjas in Pyjamas, а затем в полуфинале одолела Heroic. На пути к решающему матчу коллектив из СНГ победил Team Vitality и Gambit Esports.
						</p>
						<p>
							PGL Major Stockholm 2021 прошел с 26 октября по 8 ноября в Швеции. 24 команды разыграли призовой фонд в размере $2 млн.
						</p>
					 </div>
				</div>
				<div>
					<div className="text-spirit">
						<p>
					 		Team Spirit стала чемпионом The International 10
							по Dota 2 
					 	</p>
					 	<p>
							Team Spirit обыграла PSG.LGD в гранд-финале The International 10 (2021) по Dota 2. Встреча завершилась со счетом 3:2. За победу команда из СНГ заработала более $18,2 млн.
						</p>
						<p>
							Оба коллектива вышли в плей-офф из группы B. Spirit уступила Invictus Gaming в первом раунде виннеров. В нижней сетке команда Ярослава Miposhka Найдёнова одолела Fnatic, OG, Virtus.pro, IG и Team Secret. На пути к решающему матчу LGD победила T1, VP и Secret.
						</p>
						<p>
							The International 10 прошел с 7 по 17 октября в Бухаресте. 18 команд разыграли более $40 млн.
						</p>
					</div>
					<img src={spirit} alt="spirit" className="spirit" />
				</div>
			</div>
		</section>
	)
}
export default BlockMain