import React from 'react'

import '../../css/main.css'
// import '../../css/adaptive/adaptive_index.css'
import BlockMain from './components/block-main'

const Home = () => {
  return <BlockMain />
}

export default Home
