import React from "react"
import '../../css/main.css'
import '../../css/game.css'
import '../../css/adaptive/adaptive_game.css'
import GameBlock from "./components/game-block"
import csgo from '../../img/game/csgo.jpg'
import dota2 from '../../img/game/dota2.jpg'
import lol from '../../img/game/lol.jpg'
import fortnite from '../../img/game/fortnite.jpg'
import callofduty from '../../img/game/callofduty.jpg'
import warzone from '../../img/game/warzone.jpg'
import pubg from '../../img/game/pubg.jpg'
import over from '../../img/game/over.jpg'
import rss from '../../img/game/rss.jpg'
import hs from '../../img/game/hs.jpg'
import rl from '../../img/game/rl.jpg'
import valorant from '../../img/game/valorant.jpg'
import apex from '../../img/game/apex.jpg'
import fifa2021 from '../../img/game/fifa2021.jpg'
import smite from '../../img/game/smite.jpg'

const Game = () => {
    return (
        <section>
            <div className="container">
                <center>
                    <h4>
                        15 ЛУЧШИХ КИБЕРСПОРТИВНЫЕ ИГРЫ 2021
                    </h4>
                </center>
                <div className="textgame">
                    <p>
                        Киберспорт становится все популярнее с каждым днем и существует так много киберспортивных игр и мероприятий, что это может сбить с толку новичка. Если вы мечтаете стать профессиональным игроком в видеоигры, вам нужно обратить свое внимание на приведенные в статье киберспортивные дисциплины, они добились успеха благодаря высокому интересу со стороны окружающего их сообщества, либо благодаря сильной поддержке разработчиков.
                    </p>
                    <h5 className="h5pad">
                        КИБЕРСПОРТИВНЫЕ ДИСЦИПЛИНЫ 2021
                    </h5>
                    <p>
                        В данный список вошли игры, чей общий призовой фонд на 2020-2021 выше 100 000$ за все события. Стоит обратить внимание что статья не охватывает события второй половины года.
                    </p>
                </div>
                <GameBlock
                    ClassDiv={'size bottom1'}
                    img={csgo}
                    ClassImg={'leftimg paddingimg img1'}
                    NameGame={'Counter-Strike: Global Offensive'}
                    desc={'На данный момент самая крупная киберспортивная дисциплина. CS:GO не только держит планку популярности, но и активно набирает новую аудиторию. CS: GO, несомненно, является одной из самых влиятельных игр для развития киберспорта в мейнстриме.'}
                />
                <GameBlock
                    ClassDiv={'size bottom2'}
                    img={dota2}
                    ClassImg={'leftimg paddingimg img2'}
                    NameGame={'Dota 2'}
                    desc={'Dota 2, одна из самых популярных киберспортивных игр в мире. Основной турнир The International, является крупнейшим соревновательным игровым событием в мире и самым популярным киберспортивным шоу на Twitch. К сожалению, The International 2020 был отменен из за пандемии, на The International 2021 победили команды Team Spirit.'}
                />
                <GameBlock
                    ClassDiv={'size bottom3'}
                    img={lol}
                    ClassImg={'leftimg paddingimg img3'}
                    NameGame={'League of Legends'}
                    desc={'В 2020 году League of Legends не уступал в количестве турниров и общему призовому фонду, своему основному конкуренту, Dota 2.'}
                />
                <GameBlock
                    ClassDiv={'size bottom4'}
                    img={fortnite}
                    ClassImg={'leftimg paddingimg img4'}
                    NameGame={'Fortnite'}
                    desc={'Fortnite входит в число лучших киберспортивных игр по популярности и призовым фондам с 2019 года.'}
                />
                <GameBlock
                    ClassDiv={'size bottom5'}
                    img={callofduty}
                    ClassImg={'leftimg paddingimg img5'}
                    NameGame={'Call of Duty: Modern Warfare'}
                    desc={'Выпускаемая ежегодно, многолетняя франшиза FPS, хорошо известна своими невероятно конкурентоспособными игровыми сообществами, с профессиональными киберспортивными турнирами.'}
                />
                <GameBlock
                    ClassDiv={'size bottom6'}
                    img={warzone}
                    ClassImg={'leftimg paddingimg img6'}
                    NameGame={'Call of Duty: Warzone'}
                    desc={'Бесплатный battle royale, на основе Call of Duty: Modern Warfare, вышедший в 2020 году. Свежая и популярная киберспортивная дисциплина.'}
                />
                <GameBlock
                    ClassDiv={'size bottom7'}
                    img={pubg}
                    ClassImg={'leftimg paddingimg img7'}
                    NameGame={'PLAYERUNKNOWN’S BATTLEGROUNDS'}
                    desc={'На момент написания статьи, PUBG является лидером по выплаченному призовому фонду — более 7 мл. долларов за 2 турнира.'}
                />
                <GameBlock
                    ClassDiv={'size bottom8'}
                    img={over}
                    ClassImg={'leftimg paddingimg img8'}
                    NameGame={'Overwatch'}
                    desc={'На 2021 год тихо умирает, все ожидают выхода Overwatch 2.'}
                />
                <GameBlock
                    ClassDiv={'size bottom9'}
                    img={rss}
                    ClassImg={'leftimg paddingimg img9'}
                    NameGame={'Rainbow Six Siege'}
                    desc={'Тактический шутер от первого лица, разработанный Ubisoft и выпущенный в 2015 году, активно поддерживает и проводит киберспортивные турниры. Его главный турнир Six Invitational 2021 состоится в мае.'}
                />
                <GameBlock
                    ClassDiv={'size bottom10'}
                    img={hs}
                    ClassImg={'leftimg paddingimg img10'}
                    NameGame={'Hearthstone'}
                    desc={'Коллекционная карточная игра по мотивам вселенной Warcraft. Разработчики активно поддерживают постоянно проводимые отборочные игры, международные турниры и еженедельные соревнования верхней ступени для лучших игроков.'}
                />
                <GameBlock
                    ClassDiv={'size bottom11'}
                    img={rl}
                    ClassImg={'leftimg paddingimg img11'}
                    NameGame={'Rocket League'}
                    desc={'С одной стороны это простая аркадная гоночная игра в жанре футбола, а с другой, она является одной из самых сложных командных киберспортивных дисциплин в мире.'}
                />
                <GameBlock
                    ClassDiv={'size bottom12'}
                    img={valorant}
                    ClassImg={'leftimg paddingimg img12'}
                    NameGame={'VALORANT'}
                    desc={'Новинка, основной целью которой было заменить старенький CSGO, пока не вышло. Тем не менее, игра активно продвигает себя на киберспортивной сцене.'}
                />
                <GameBlock
                    ClassDiv={'size bottom13'}
                    img={apex}
                    ClassImg={'leftimg paddingimg img13'}
                    NameGame={'Apex Legends'}
                    desc={'EA, активно проводят турниры ALGS для Apex Legends, транслируя все соревнований на нескольких видеоплатформах, в том числе на YouTube и Twitch.'}
                />
                <GameBlock
                    ClassDiv={'size bottom14'}
                    img={fifa2021}
                    ClassImg={'leftimg paddingimg img14'}
                    NameGame={'FIFA 21'}
                    desc={'В 2020 году для FIFA 20, было проведено около 40 турниров. Будем надеяться, что FIFA 21 не опустит планку.'}
                />
                <GameBlock
                    ClassDiv={'size bottom15'}
                    img={smite}
                    ClassImg={'leftimg paddingimg img15'}
                    NameGame={'SMITE'}
                    desc={'SMITE — MOBA от 3го лица, в которой активно проводится различные турниры для профессиональных игроков.'}
                />
            </div>
        </section>
    )
}
export default Game