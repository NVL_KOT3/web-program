import React from "react"

const GameBlock = ({ ClassDiv, img, ClassImg, NameGame, desc }) => {
    return (
        <div className={ClassDiv}>
            <img src={img} alt="" className={ClassImg} />
            <div>
                {NameGame}
                <br />
                <br />
                {desc}
            </div>
        </div>
    )
}
export default GameBlock