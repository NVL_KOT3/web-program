import React from 'react'
import '../../css/main.css'
// import '../../css/adaptive/adaptive_biography.css'
import MarkellofBlock from './components/markelloff-block'
import ZeusBlock from './components/zeus-block'
import Ceh9Block from './components/ceh9-block'
import EdwardBlock from './components/edward-block'
import StarixBlock from './components/starix-block'

const Boigraphy = () => {
  return (
    <section className="about">
      <div className="container">
        <MarkellofBlock />
        <ZeusBlock />
        <Ceh9Block />
        <br />
        <EdwardBlock />
        <br />
        <StarixBlock />
        <div className="footer"></div>
      </div>
    </section>
  )
}
export default Boigraphy
