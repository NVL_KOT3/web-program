import React from "react"
import starix from '../../../img/starix.jpg'

const StarixBlock = () => {
    return (
        <><div>
            <img src={starix} alt="starix" className="stariximg leftimg" />
        </div>
        <div className="bleft-text">
                <p>
                    Сергей "Starix" Ищук
                </p>
                <p>
                    Сергей покоряет про-сцену с 2004 года. Выступал в дисциплине Counter-Strike 1.6, представлял коллективы Arbalet.UA, KerchNET и A-Gaming. В 2010-м был приглашен в Natus Vincere, несколько раз стал чемпионом мира. В 2010-м вошел в топ-4 рейтинга лучших игроков мира по версии портала HLTV, в 2011-м - занял 20 строчку.
                </p>
                <p>
                    В 2012-м в составе NaVi перешел в CS:GO, в 2014-м помог коллективу победить на StarLadder StarSeries IX, занять 3-4 место на DreamHack Winter 2014. В марте 2015-го занял должность тренера состава и проработал до марта 2017-го. В мае устроился в Team Spirit в качестве игрока, покорил Binary Dragons Prestige LAN. В октябре ушел в инактив, и не выступал до февраля 2018-го. В мае присоединился к k1ck eSports Club, стал лучшим на Rivalry CIS Invitational.
                </p>
                <p>
                    В 2019-2020 году выступал за различные миксы, но и близко не подошел к прежним результатам. В сентябре 2020-го стал наставником команды Hard Legion.
                </p>
        </div></>
    )
}
export default StarixBlock