import React from 'react'
import markeloff from '../../../img/markeloff.jpg'

const MarkellofBlock = () => {
	return (
		<><div>
			<img src={markeloff} alt="markeloff" className="markeloffimg leftimg" />
		</div><div className="bleft-text">
				<p>
					Егор "markeloff" Маркелов
				</p>
				<p>
					Егор Вадимович Маркелов (12 февраля 1988; Днепропетровск) — украинский профессиональный киберспортсмен в дисциплине CS. Лучший игрок мира в 2010 году по версии сайта HLTV.org. Участник первых двенадцати мейджоров по CSGO. Чемпион Европы Wesg 2017 года в составе Team Ukraine. Не участвовал в профессиональных матчах с 2018 года.
				</p>
				<p>
					В 2005 году Егор познакомился с Иваном Шевцовым, который, в свою очередь, привел Маркелова в команду DTS.Chatrix. Началом профессиональной карьеры можно считать первый выездной турнир Егора — КиберМетель 2005, на котором DTS вышли в полуфинал, но проиграли команде A-Gaming. Через некоторое время из DTS образовалась новая команда — HellRaisers — на тот момент ещё слабая команда с игроками аматорами. Со временем, HellRaisers, выиграв несколько турниров, распадаются. Через год команда в полном составе вернулась, вновь под тэгом DTS.Chatrix.
				</p>
			</div><div className="btext">
				<p>
					Состав DTS на 2008 год:
				</p>
				<p>
					Иван «Johnta» Шевцов (Днепропетровск)
					<br />
					Егор «markeloff» Маркелов (Днепропетровск)
					<br />
					Егор «pops» Зиновьев (Днепропетровск)
					<br />
					Валентин «Valentinich» Кравченко (Донецк)
					<br />
					Кирилл «ANGE1» Карасев (Киев)
				</p>
				<p>
					В 2009 году прошёл турнир KODE5 Spain, на котором, дойдя до финала, DTS проиграли испанской команде mTw.
				</p>
				<p>
					После распада он играл в топовых украинских коллективах, включая pro100, KerchNET и Arbalet.UA. В 2009 году Егору предложили перейти в новую команду под названием Na’Vi .Именно в этой команде Маркелов добился наибольших успехов в период с 2009 по 2012 годы, выступая за мультигейминговый проект — Natus Vincere. В составе «Рождённых Побеждать» выиграл четыре чемпионата мира. Украинский проект стал всемирно известным.
				</p>
				<p>
					14 июня 2010 года в здании Кабинета Министров состоялась встреча премьер-министра Украины Николая Азарова и команды Natus Vincere. На ней в том числе обсуждались вопросы развития индустрии информационных технологий на Украине.
				</p>
				<p>
					Сумма призовых в 2010 году — $225 080, что является наивысшим достижением за всю историю профессионального Counter-Strike. Предыдущие рекорды принадлежали шведским командам SK Gaming ($183 000 в 2003 году) и fnatic ($189 000 в 2009 году).
				</p>
				<p>
					В течение трёх лет коллектив считался самым стабильным, однако после перехода на новую игровую дисциплину — Counter-Strike: Global Offensive, команда в течение 2013 года не показала высоких результатов. Состав с ноября 2012 — июль 2013 года:
				</p>
				<p>
					Даниил «Zeus» Тесленко (Харьков)
					Сергей «Starix» Ищук (Киев)
					Арсений «ceh9» Триноженко (Львов)
					Иоанн «Edward» Сухарёв (Харьков)
					Егор «Markeloff» Маркелов (Днепропетровск)
				</p>
				<p>
					11 мая 2020 года выступил в старом составе Natus Vincere 2010. Вместе с Zeus, Edward, ceh9, starix обыграли состав стримеров из СНГ со счетом 3:1 в BO5 матче. 2 карты были сыграны в Counter Strike 1.6. Состав был назван «Reversus Vincere», что означало «вернувшиеся побеждать».
				</p>
			</div></>
	)
}
export default MarkellofBlock