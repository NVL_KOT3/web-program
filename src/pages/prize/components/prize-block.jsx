import React from "react"

const PrizeBlock = ({ top, img, desc }) => {
    return (
        <><center>
            <h5 className="h5name">
                {top}
            </h5>
        </center>
        <center>
            <img src={img} alt="" className="imgwidth" />
        </center>
        <p className="pmargin">
            {desc}
        </p></>
    )
}
export default PrizeBlock