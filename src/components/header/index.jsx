import React from 'react'

import { Link } from 'react-router-dom'
import menu from '../../img/menu.svg'
import close from '../../img/close.svg'
import cross from '../../img/cross.png'

const Header = () => {
  const [open, setOpen] = React.useState(false)

  return (
    <header className="header">
      <div className="container">
        <div className="nav">
          <div className="name">CyberPlanet</div>
          <ul class="menu">
            <li>
              <Link to="/">Главная</Link>
            </li>
            <li>
              <Link to="/tournaments">Турниры</Link>
            </li>
            <li>
              <Link to="organizations">Организации</Link>
            </li>
            <li>
              <Link to="biography">Биография игроков</Link>
            </li>
            <li>
              <Link to="prize">Призовые фонды</Link>
            </li>
            <li>
              <Link to="game">Игры</Link>
            </li>
          </ul>
          <button className="menu-open" style={{ zIndex: 102 }}>
            {open ? (
              <img src={close} alt="" />
            ) : (
              <img src={menu} alt="menu open" onClick={() => setOpen(!open)} />
            )}
          </button>
          <div
            style={{
              display: open ? 'block' : 'none',
              position: 'fixed',
              backgroundColor: '#2F2F2F',
              //   marginTop: 65,
              height: '100vh',
              width: '100vw',
              zIndex: 101,
              right: 0,
              overflowY: 'hidden',
            }}
          >
            <header className="header">
              <div className="container">
                <div className="nav">
                  <div className="name">CyberPlanet</div>
                  <button className="menu-open" style={{ zIndex: 102 }}>
                    {open ? (
                      <img src={cross} alt="" style={{ marginLeft: "5px" }} onClick={() => setOpen(!open)}/>
                    ) : (
                      <img
                        src={menu}
                        alt="menu open"
                        onClick={() => setOpen(!open)}
                      />
                    )}
                  </button>
                </div>
              </div>
            </header>
            <div style={{ listStyleType: 'none', textAlign: 'center', lineHeight: '2.5', marginTop: '10px' }}>
            <li>
              <Link to="/">Главная</Link>
            </li>
            <li>
              <Link to="/tournaments">Турниры</Link>
            </li>
            <li>
              <Link to="organizations">Организации</Link>
            </li>
            <li>
              <Link to="biography">Биография игроков</Link>
            </li>
            <li>
              <Link to="prize">Призовые фонды</Link>
            </li>
            <li>
              <Link to="game">Игры</Link>
            </li>
            </div>
          </div>
        </div>
      </div>
    </header>
  )
}
export default Header
